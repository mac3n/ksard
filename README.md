# ksard

This is an attempt at mesoscale parallelism,
as well as another application of random access text files.

`ksard` runs parallel instances of a text filter on shards of its input file.
The input must be a regular file (an input stream would have to to be processed sequentially).
The input is memory-mapped and partitioned into roughly equal shards on line boundaries.
Each shard is piped to a filter process on stdin in parallel, with the resulting stdout collected in a temporary file.
The shard results are dumped in order to `ksard`'s stdout.

For example,

	ksard -P20 -- grep ^xge. < names-2020-05-25

forks 20 instances of `grep ^xge`, each on a partition of the text file `names-2020-05-25``.
Output is in-order, and is the same as it would be for a single process.

Each filter instance runs with the environment variable KSARD
set to its shard number.

## speedup

Most text filters, like `grep` above, are limited by I/O bandwidth, not processing time,
so parallel processing has no speedup.
Speedup is only possible when there is significant processing on each line of input.
A more expensive filter on a 60GB input file saw 6.4x speedup with 10 processes.

To run a filter command, `ksard` has to pipe the input shard to the filter process.
The internals of `ksard` can bypass this for a filter subroutine that processes memory-mapped text.

## usage

	Usage: ksard [OPTION...] arg...
	run parallel text filters.

	  -P count                   count for filter processes.
	  -T tempdir                 output tempfile directory
	  -v                         increase verbosity (may be repeated)
	  -?, --help                 Give this help list
	  --usage                    Give a short usage message

## examples

	ksard -P10 perl lookups.pl

runs a perl script on each shard to do expensive lookups on stdin

	for i in $(seq -w 0 3 100); do echo -e "$i\tfizz"; done > A
	for i in $(seq -w 0 5 100); do echo -e "$i\tbuzz"; done > B
	ksard -P10 < A -- join - B

runs 10 text inner `join(1)`s in parallel.
The `--` option tells `ksard` that all remaining args are part of the `join` filter.

	ksard -P10 -v -- sh -c 'cat > split-$KSARD'

is a strange way to split stdin on line boundaries into 10 roughly equal fragments.

# build

Run `make tests`.
Run `make`

# issues

`ksard` uses unlinked "disappearing" temporary files.
This may not work on some network file systems.

Not all file systems support `mmap` on the input file.
Some filesystems truncate large writes from `mmap` files at 0xfffff000.
`ksard` correctly handles this.

# see also

*	`xargs -P`	runs `xargs(1)` lines in parallel
*	GNU `parallel` does it all
