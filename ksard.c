#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "ksard.h"

/* align text offset at the next line boundary after a split */
static size_t
align(const char *text, size_t size, size_t split) {
	const char *eol;
	if (split == 0) {
		/* first line */
		return 0;
	}
	if (size <= split) {
		return size;
	}
	eol = memchr(text + split, '\n', size - split);
	if (!eol) {
		/* no more lines */
		return size;
	}
	return eol - text + 1;
}

/* fork a process at each shard, starting at the last
 * splitting the input at line boundaries
 * each iteration of this loop runs in a separate process
 * slices are aligned to a fixed size,
 * but each process gets a split after the line containing the slice point.
 */
int
ksard(const char *text, size_t size, int shards, filtproc filt) {
	/* round up shard slice */
	size_t slices = (size + shards - 1) / shards;
	/* start, end of initial slice */
	size_t slice0, slice1 = size;
	/* line-aligned slice */
	size_t split0 = 0, split1 = size;
	/* prev shard process */
	int ppid = 0;
	int i, shard;
	int x;
	/* loop forking off processes */
	for (i = 0; i < shards; i++) {
		slice0 = slices < slice1 ? slice1 - slices : 0;
		split0 = align(text, split1, slice0);
		if (i + 1 == shards) {
			/* no predecessor for last fork */
			break;
		}
		ppid = fork();
		failng(ppid);
		if (ppid) {
			/* child is prev */
			break;
		}
		slice1 = slice0;
		split1 = split0;
	}
	/* forked child (or root process at last shard) */
	shard = shards - i - 1;
	note(0, "[%d] pid %d split %ld[%ld]\n", shard, ppid, split0, split1 - split0);
	x = filt(shard, text + split0, split1 - split0, ppid);
	if (x) {
		note(0, "[%d] exit %#x\n", shard, x);
	}
	if (i) {
		/* child process */
		exit(x);
	}
	return x;
}

/* just exec the filter directly with no forking */
void
ksard0(char *filtv[]) {
	note(1, "exec %s", filtv[0]);
	failng(execvp(filtv[0], filtv));
}


/* pipeline a filter from stdin on an input range */
int
filtpipe(int dstfd, int shard, const char *text, size_t size, char *filtv[]) {
	/* filter stdin pipe */
	int pfds[2];
	int fpid;
	int fexit = 0;
	/* create subprocess with piped stdin and tmp stdout */
	failng(pipe(pfds));
	fpid = fork();
	failng(fpid);
	if (!fpid) {
		/* subprocess remaps pipe read and tmp and closes pipe write */
		fdclose(pfds[1]);
		fdremap(fdstdin, pfds[0]);
		fdremap(fdstdout, dstfd);
		note(1,"pipe [%d] exec %s\n", shard, filtv[0]);
		failng(execvp(filtv[0], filtv));
	}
	/* write shard to subprocess, then close pipe */
	fdclose(pfds[0]);
	fdmmput(pfds[1], text, size);
	fdclose(pfds[1]);
	/* wait for filter to finish */
	note(1, "pipe [%d] filt wait %d\n", shard, fpid);
	waitpid(fpid, &fexit, 0);
	note(0, "pipe [%d] filt exit %#x\n", shard, fexit);
	return WEXITSTATUS(fexit);
}

/* set environment for filter */
void
filtenv(const char *name, int shard) {
	char s[64];
	snprintf(s, sizeof s, "%d", shard);
	note(1, "pipe[%d] %s=%s\n", shard, name, s);
	failng(setenv(name, s, 1));
}

/* serialize with prev shard */
int
filtsync(int shard, int ppid) {
	int pexit = 0;
	if (ppid) {
		note(1, "pipe [%d] prev wait %d\n", shard, ppid);
		waitpid(ppid, &pexit, 0);
		note(1, "pipe[%d] prev exit %#x\n", shard, pexit);
	}
	return WEXITSTATUS(pexit);
}

#ifdef TEST
#include <assert.h>

static char testtext[10] = "some\ntext\n";

/* test line alignment */
static void
testalign(void) {
	int i;
	assert(align(testtext, sizeof testtext, 0) == 0);
	for (i = 1; i < 5; i++) {
		assert(align(testtext, sizeof testtext, i) == 5);
	}
	for (i = 5; i < 10; i++) {
		assert(align(testtext, sizeof testtext, i) == 10);
	}
}

/* test filter pipeline */
static void
testpipefilt(void) {
	static char *filtv[3] = {"tee", "/dev/tty", NULL};
	/* capture output */
	int dstfd = fdtmp("/tmp/test-XXXXXX");
	/* split input */
	int size = align(testtext, sizeof testtext, 1);
	/* run filter */
	int exit = filtpipe(dstfd, 17, testtext, size, filtv);
	/* check */
	assert(exit == 0);
	assert(fdsize(dstfd) == size);
	fdclose(dstfd);
}

/* test filter callback */
static int
testksard1(int shard, const char *text, size_t size, int ppid) {
	int pexit = 0;
	/* valid text */
	assert(testtext <= text && text + size <= testtext + sizeof testtext);
	if (ppid) {
		fprintf(stderr, "test [%d] wait %d\n", shard, ppid);
		waitpid(ppid, &pexit, 0);
		fprintf(stderr, "test [%d] exit %#x\n", shard, pexit);
		assert(WEXITSTATUS(pexit) == shard - 1);
	}
	return shard;
}

/* test shard forking */
static void
testksard(void) {
	/* do the forks */
	int n = 4;
	ksard(testtext, sizeof testtext, n, testksard1);
}

int main() {
	verbose = 4;
	fprintf(stderr, "test align\n");
	testalign();
	fprintf(stderr, "test pipefilt\n");
	testpipefilt();
	fprintf(stderr, "test ksard\n");
	testksard();
	fprintf(stderr, "passed\n");
	return 0;
}

#endif
