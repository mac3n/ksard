#include <stdlib.h>
#include <string.h>
#include <argp.h>

#include "ksard.h"

/* process count */
static int	shards = 0;
static int
shardarg(char *arg) {
	char *eon = arg;
	int n = strtoul(arg, &eon, 0);
	if (*eon) {
		fail("bad process count: %s\n", arg);
	}
	return n;
}

/* output temporary dir */
static char	*tmpdir = NULL;

/* filter command */
static int	filtc = 0;
static char	**filtv = NULL;
static void
filtarg(char *arg) {
	filtc++;
	filtv = realloc(filtv, (filtc + 1) * sizeof *filtv);
	filtv[filtc - 1] = arg;
	filtv[filtc] = NULL;
}

/* our CLI options */
static struct argp_option	opts[] = {
	{0, 'P', "count", 0, "count for filter processes."},
	{0, 'T', "tempdir", 0, "output tempfile directory"},
	{0, 'v', 0, 0, "increase verbosity (may be repeated)"},
	{0}
};

/* Called with argv options. */
static error_t
optsarg(int k, char *arg, struct argp_state *state) {
	switch (k) {
	case 'P':
		shards = shardarg(arg);
		return 0;
	case 'T':
		tmpdir = arg;
		return 0;
	case 'v':
		verbose++;
		return 0;
	case ARGP_KEY_ARG:
		filtarg(arg);
		return 0;
	case ARGP_KEY_END:
		if (!state->arg_num) {
			argp_usage(state);
		}
		return 0;
	case ARGP_KEY_INIT:
		return 0;
	default:
		return ARGP_ERR_UNKNOWN;
	};
}

static struct argp	argp = {
	opts,
	optsarg,
	"arg...",
	"run parallel text filters."
};

/* per-shard filter
 * run filter on shard into tmpp file
 * dump tmp to stdout when prev proc exits
 */
static char	*tmpl = "/ksard-XXXXXX";
static int
filt(int shard, const char *text, size_t size, int ppid) {
	/* output to tmp */
	int tmp = fdtmp(tmpl);
	int x, px;
	filtenv("KSARD", shard);
	/* run pipes in parallel */
	x = filtpipe(tmp, shard, text, size, filtv);
	/* wait for prev output */
	px = filtsync(shard, ppid);
	/* dump output */
	size = fdsize(tmp);
	text = fdmmap(tmp, size);
	fdmmput(fdstdout, text, size);
	fdclose(tmp);
	/* propagate nonzeroexit status */
	return x ? x : px;
}

int
main(int argc, char *argv[]) {
	const char *text;
	size_t size;
	int x;
	/* options */
	argp_parse(&argp, argc, argv, 0, 0, NULL);
	if (!shards) {
		/* degenerate case */
		note(0, "no forks\n");
		ksard0(filtv);
	}
	/* mmap file for random access */
	size = fdsize(fdstdin);
	text = fdmmap(fdstdin, size);
	/* use temp files for shard results */
	if (!tmpdir) {
		/* $TMPDIR or /tmp */
		tmpdir = getenv("TMPDIR");
		if (!tmpdir) {
			tmpdir = "/tmp";
		}
	}
	tmpl = strcat(strcpy(malloc(strlen(tmpdir) + strlen(tmpl) + 1), tmpdir), tmpl);
	/* fork and filter */
	x = ksard(text, size, shards, filt);
	free(tmpl);
	return x;
}
