#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <argp.h>

#include "ksard.h"

int verbose = 0;

/* Report to user. */
void
note(int v, const char *why, ...) {
	va_list ap;
	va_start(ap, why);
	if (v < verbose) {
		vfprintf(stderr, why, ap);
	}
	va_end(ap);
}

/* Report to user ... and die */
void
fail(const char *why, ...) {
	va_list ap;
	va_start(ap, why);
	vfprintf(stderr, why, ap);
	va_end(ap);
	abort();
}

/* fail system call if negative */
void
failng(int err) {
	if (err < 0) {
		fail("%s\n", strerror(errno));
	}
}


