#!/bin/bash
# run tests

# stop on failure
set -e
trap "echo failed" ERR

testing="$*"
testing=${testing:-"./ksard -T."}
echo "testing $testing"
testin=$(mktemp --suffix -ksard)
testout=$(mktemp --suffix -ksard)

echo -n "empty file "; ls -sh $testin
$testing cat < $testin > $testout
cmp $testin $testout
$testing -P4 -- cat < $testin > $testout
cmp $testin $testout

echo -n "small file "; seq 0 9 > $testin
ls -sh $testin
for p in 0 1 4 12
do	echo $testing -P$p
	$testing -P$p -- cat < $testin > $testout
	cmp $testin $testout
done

echo -n "larger file "; seq 0 999999 > $testin
ls -sh $testin
for p in $(seq 0 25)
do	echo -P$p
	$testing -P$p -- cat < $testin > $testout
	cmp $testin $testout
done

rm $testin $testout
echo "passed"
