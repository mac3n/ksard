/* internal interfaces */

/*
 * errors and debugs
 */
extern	int verbose;
/* Report to user. */
void	note(int v, const char *why, ...);
/* ... and die */
void	fail(const char *why, ...);
/* check system call status */
void	failng(int err);

/*
 * things we do with open files
 */
/* get the file size */
size_t	fdsize(int fd);
/* memory-map for random access */
const char	*fdmmap(int fd, size_t size);
/* clean up fd */
void	fdclose(int fd);
/* remap a FD to optional fd and close the original */
void	fdremap(int fd1, int fd0);
/* make a temp file */
int		fdtmp(const char *tmpl);
/* copy mmap to file */
void	fdmmput(int fd, const char *text, size_t size);
/* stdio */
enum {
	fdstdin = 0,
	fdstdout = 1
};

/*
 * sharding
 */
/* fork callback */
typedef int	filtproc(int shard, const char *text, size_t size, int ppid);
/* pipe as stdin through filt */
int		filtpipe(int dstfd, int shard, const char *text, size_t size, char *filtv[]);
/* set environment for filter */
void	filtenv(const char *name, int shard);
/* serialize with prev shard */
int		filtsync(int shard, int ppid);
/* do the forking */
int		ksard(const char *text, size_t size, int shards, filtproc filt);
/* exec filter directly with no forking */
void	ksard0(char *filtv[]);
