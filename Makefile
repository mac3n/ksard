
CFLAGS=-Wall -Os

ksard: note.o fdop.o ksard.o main.o

.c.o:	ksard.h

# unit tests

.SUFFIXES: .test

.c.test:	ksard.h note.o
	cc $(CFLAGS) -DTEST -g -o $@ $^
	./$@

fdop.test: note.o
ksard.test: note.o fdop.o

test:	note.o fdop.test ksard.test ksard
	./tests.sh

# maintenance

clean:
	-rm *.o *.test
