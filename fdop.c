#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "ksard.h"

/* get the file size */
size_t
fdsize(int fd) {
	struct stat s;
	failng(fstat(fd, &s));
	if ((s.st_mode & S_IFMT) != S_IFREG) {
		fail("not a regular file\n");
	}
	return s.st_size;
}

/* memory-map for random access */
const char
*fdmmap(int fd, size_t size) {
	const char *m = "";
	if (size == 0) {
		/* linux won't mmap an empty file */
		return m;
	}
	m = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
	if (m == MAP_FAILED) {
		failng(-errno);
	}
	return m;
}

/* close, provided for convenience and cleaner interface */
void
fdclose(int fd) {
	failng(close(fd));
}

/* remap a FD to another fd and close the original */
void
fdremap(int fd1, int fd0) {
	if (fd1 == fd0) {
		/* stet */
		return;
	}
	if (0 <= fd1) {
		/* move to new fd */
		failng(dup2(fd0, fd1));
	}
	/* close old fd */
	fdclose(fd0);
}

/* make a temp file */
int
fdtmp(const char *tmpl) {
	char *t = strdup(tmpl);
	int fd;
	if (!t) {
		fail("strdup fail\n");
	}
	fd = mkstemp(t);
	failng(fd);
	note(1, "tmp %s\n", t);
	unlink(t);
	free(t);
	return fd;
}

/* copy mmap to file
 * we loop because of cases where a >2GB write
 * gets truncated at 0xfffff000 with no error
 * possibly related to vmsplice(2)
 */
void
fdmmput(int fd, const char *text, size_t size) {
	while (size) {
		size_t w = write(fd, text, size);
		if (w == (size_t)-1) {
			failng(-errno);
		}
		note(1, "write %ld/%ld\n", w, size);
		text += w;
		size -= w;
	}
}

#ifdef TEST
#include <stdio.h>
#include <assert.h>

char tdata[10] = "012345678\n";

int
main(int argc, char *argv[]) {
	int tfd0, tfd1;
	verbose = 2;
	/* temp files */
	tfd0 = fdtmp("/tmp/test-XXXXXX");
	tfd1 = fdtmp("/tmp/test-XXXXXX");
	/* get expected size */
	fdmmput(tfd0, tdata, sizeof tdata);
	assert(fdsize(tfd0) == sizeof tdata);
	/* remap to stdio with correct size */
	fdremap(fdstdin, tfd0);
	assert(fdsize(fdstdin) == sizeof tdata);
	fdremap(fdstdout, tfd1);
	assert(fdsize(fdstdout) == 0);
	/* mmap copy stdin to stdout with correct size */
	fdmmput(fdstdout, fdmmap(fdstdin, sizeof tdata), sizeof tdata);
	assert(fdsize(fdstdout) == sizeof tdata);
	fprintf(stderr, "passed\n");
	return 0;
}
#endif
